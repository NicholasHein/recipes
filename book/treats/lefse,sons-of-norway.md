# Lefse (Sons of Norway)

**Time:** 2 days

## Ingredients

* 5 lbs. Potatoes
* 12 tbsp. Butter
* 4 tbsp. Half &amp; Half
* 3 C. Flour
* 2 tsp. Salt
* 3 tbsp. Sugar

## Instructions

**Prepare potatoes (1 day):**

1. Peel and boil potatoes
2. Rice the potatoes and let **cool in a refrigerator overnight**

**Prepare patties (approx. 1 hr):**

1. Melt together the butter with half &amp; half
2. In a separate bowl, combine flour, salt, and sugar
3. Knead potatoes, butter/half &amp; half, and dry ingredients until mixed
   *(usually, knead 10-15 times)*
4. Using a 1/3 C. measuring cup, scoop out and pat together into a smooth patty
   *(make sure there are no cracks on the edges)*
5. Store in refrigerator until you need them *(or at least half an hour)*

**Make the Lefse:**

1. Heat grittle to 500 degrees
2. Flour a surface
3. Roll patty out as thin as possible without falling apart *(usually until
   semi-translucent)*
4. Using a Lefse stick, carefully lift and place on a grittle
5. Flip Lefse once light brown spots start to appear *(usually about 3
   minutes)*
6. Cook the other side for approx. 2 more minutes or when light brown spots
   appear
7. Move finished Lefse to a plate and cover with a dish towel while you are
   finishing the rest

   **Note:** while working, if the edges of any finished Lefse seem dry,
   lightly moisten the edges with your fingers.

### Original Recipe

![Original Sons of Norway Lefse Recipe](../images/lefse,sons-of-norway_original-recipe.jpg)

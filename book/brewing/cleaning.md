# Cleaning

## Sanitizers

**Star San Acid-Based Sanitizer**
* Works very well!
* Foam helps increase coverage
* No-rinse
* Reusable

**BTF Iodophor Sanitizer (Titratable Iodine)**
* No-rinse at 12.5 PPM for brewing

  1:10 sanitizer (oz.) to water (gallon) ratio (1/2 oz. sanitizer per 5
  gallons, 1 tsp. sanitizer per 1.5 gallons).
* 2 min. contact time, air dry (at least 10 min.)
* Non-corrosive and safe for skin and hands
* **Will stain clothes!**
* Reusable as long as the solution is still orange/amber[^iodophor-reuse]

**Others**
* PBW is supposedly a favorite
* Supposedly you can be real cheap and use Oxiclean[^oxiclean]

[^oxiclean]: https://www.experimentalbrew.com/2015/05/18/craftmeister-cleaners-the-picturing-carboy-test/
[^iodophor-reuse]: https://www.baderbrewing.com/content/simplifying-cleaning-and-sanitizing-home-brewers

## Carboy

1. Wash off particulate.  If possible, use a jet of water like from a bottle
   washer attached to a faucet.  Otherwise, soak with water that is less than
   125F and agitate (to quote a pamphlet I received from "The Vintage Shop",
   "remember you are sanitizing, not sterilizing").  If absolutely necessary,
   use a soft **non-abrasive** cloth to gently rub it off.  Be careful to not
   scratch the inner surface!
2. Fill your sanitizer bucket with warm water (less than 125F).  Transfer to
   the carboy
3. Add sanitizer and stir

> [!TIP]
> Now would be a good time to check the spigot stopper to be sure it
> has a good seal.  You do not want to be surprised by this later on after
> you have transferred the wort... Trust me

4. Agitate to get every surface wet. Surfaces don't have to be submerged the
   whole time to be sanitized[^wet-surfaces]
5. After the required amount of time, transfer back to sanitizer bucket
6. Let carboy air dry

[^wet-surfaces]: https://homebrew.stackexchange.com/a/4582

## Other Stuff

Fill a spray bottle with some sanitizer.  This is useful for spraying equipment
as needed and for spraying your hands before handling ingredients (such as when
dry hopping).

**Soap &amp; Water**
* Everything except carboy unless absolutely necessary

**Boil**
* Hop bags
* Bottle caps (when needed)

**Immerse in sanitizer**
* Everything else (after washing of course)

# Supplies &amp; Equipment Checklist

## Checklist

### Restock

* [ ] Ingredient Kit
* [ ] Brewer Yeast
* [ ] Bottle Caps
* [ ] Sanitizer
  * Star San
  * BTF Iodophor Sanitizer
* [ ] Cleaning Tablets
  * CraftMeister Keg &amp; Carboy Cleaning Tablets
* [~] Clarifier Tablets
  * BrewMaster Whirlfloc Clarifier
* [ ] Bottles (48 12 oz., non-twist)
* [ ] Corn sugar for bottling

### Day Before Brew Day

* [ ] Sanitizer (Star San)
* [ ] Cleaning Tablets
* [ ] Bucket (6 gallon)
* [ ] Brewing Kettle (5 gallon)
* [ ] Mixing spoon
* [ ] Mesh bags (x3)
* [ ] Carboy (Fermonster)
  * [ ] Air lock
  * [ ] Stopper
* [ ] Vinyl Tubing
* [ ] Thermometer
* [ ] Hydrometer
* [ ] Hydrometer Jar

### Brew Day

* [ ] Ingredient Kit
* [ ] Brewers Yeast

### Day After Brew Day

* [ ] Bottle Brush

### Bottling

* [ ] Bottle Brush
* [ ] Bottle Filler
* [ ] Bottle Capper
* [ ] Bottle Caps
* [ ] Bottles (48 12 oz., non-twist)

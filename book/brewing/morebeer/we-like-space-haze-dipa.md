# We Like Space Haze DIPA

* **Kit Type:** LME
* **Boil Time:** 90 min.
* **Boil Type:** Partial Boil

## Instructions

### Brewing

* [ ] Add **2-3 gallons of water** to the 5 gallon kettle and turn on heat
* [ ] Add the malts (**3 lbs. flaked oats** and **1 lb. white wheat**) to mesh bags and insert in water
* [ ] After *30 min.*, when the water reaches *170F*, remove malts

> [!NOTE]
> If the water reaches 170F too quickly, turn off the heat and let steep
> until 30 min. has passed.

* [ ] Heat the water to a boil
* [ ] Turn off the heat
* [ ] Stir in the **7 lbs. Liquid Malt Extract (LME)**
* [ ] Bring back to a boil

> [!WARNING]
> Beware of the heat break!
>
> When it is close to boiling over, turn down the heat.  When the foam drops,
> turn the heat back up and bring back to a boil.

* [ ] **90 min:** Boil for *60 min.*
* [ ] Lower temperature to *between 170F and 185F* (a low boil) to ensure
  convection keeps the wort flowing
* [ ] **30 min:** Add whirlpool hops (**2 oz. Citra, 1 oz. El Dorado**) in mesh
  bags
* [ ] **15 min:** Add **1 lb. Corn Sugar** and **1 lb. Maltodextrin** and stir
* [ ] **Pause the timer** and turn off the burner.  Stir in the sugars until
  completely dissolved
* [ ] Bring back to a boil and **resume the timer**
* [ ] **0 min:** Remove from heat and cover

### Preparation

> [!IMPORTANT]
> Remember, air is now the enemy!  "Hot side" airation can cause other microbes
> to enter the batch, which has a chance of out-competing the yeast before that
> is added. When in doubt, sanitize everything!

* [ ] Plug a sink and place covered pot in it
* [ ] Fill sink with water and add ice if possible
* [ ] When the temperature reaches *130F*, transfer wort to the carboy by
  siphoning it out with a sanitized tube.  Make sure the tube does not reach
  the bottom of the pot to prevent the sediment from the bottom from being sucked
  up.

> [!CAUTION]
> When sucking on the end of the tube while siphoning, do your absolute best to
> not get saliva on the end. Just use your lips!
>
> Do your absolute best to make sure the siphon is placed in a way that allows
> the liquid to enter the carboy smoothly with minimal splashing.

* [ ] Top off the carboy with cold tap water until it contains *5 gallons*

> [!NOTE]
> If you use your tap water, be sure it doesn't have any strong taste to it.
> If it does, it may be best to go out and buy some tasteless water.

* [ ] Seal the carboy with its stopper
* [ ] Record the initial volume in the fermenter
* [ ] Prepare **Lallemand Dry American East Coast Ale Yeast**
  * [ ] Warm water to *85-96F* in microwave
  * [ ] Sprinkle in **11 g. yeast**
  * [ ] After *15 min.*, stir briefly
  * [ ] Let sit for *5 min.*
* [ ] Add  to **3.5 oz. water** (100 mL)
* [ ] Once the temperature reaches *70-80F*, record the original gravity by
  draining some wort from the spigot into the hydrometer jar (it only needs to
  be half filled).

> [!TIP]
> Since you can't put the sample back into the carboy, this is your chance to
> get a preview of the taste!

* [ ] Add the **yeast** and record the start of the lag time

> [!TIP]
> When adding the yeast, you want to add a ton of air as well!  Pour from a
> higher hight to cause splashing.  You can even stir and/or pick up &amp; tilt
> the carboy to swirl it around.
>
> Oxygen is now your friend!  When there is oxygen in the batch, the yeast use
> all their energy to reproduce.  Increasing the population quickly helps
> ensure it can out-compete any other microbes that may be present.  It's also
> necessary because it'll help the beer ferment faster.  Once the yeast run out
> of oxygen, they stop reproducing as much and start fermenting.  So you want a
> lot to begin with!

* [ ] Place the fermenter in a dark spot in a room with a temperature that will
  remain consistenly around *65-70F*
* [ ] Monitor the air lock and record the total lag time when you see
  fermentation

> [!NOTE]
> This could take 3-4 hours or 1-2 days

### Fermentation

* [ ] **Day 0:** let ferment for *7 days* or at least until you stop seeing
  many bubbles in the air lock
* [ ] **Day 7:** let settle for *4 days*
* [ ] **Day 11:** Clean and boil mesh bags, and sanitize your hands before
  filling them with **3 oz. Citra**, **3 oz. El Dorado**, and **3 oz. Mosaic**.
  Carefully place dry hops in the carboy and let sit for *3 days*.
* [ ] **Day 14:** Take a final gravity and bottle

## Outcome

* **Brew Date:** `   `/`   `/`   `
* **Rating:** `   `/5
* **Gallons In Fermenter:** `    `g.
* **Original Gravity:** `      ` (est. 1.080 - 1.085)
* **Temp. of Wort at Pitch:**
* **Lag Time:** `   ` d. `     ` h.
* **Days in Fermenter:** `   ` d.
* **Final Gravity:** `     `
* **ABV:** `     `% (est. 8.3%)

## Notes

* When I ordered the yeast, MoreBeer sent me 11g of **Lallemand Dry American
  East Coast Ale Yeast**.  I was originally worried, since it mentioned that it
  had a pitching rate of 1.0g/L, which comes out to roughly 2.9 gallons.  But
  it seems to be enough!

## References

* [MoreBeer](https://www.morebeer.com/products/space-hazy-dipa-extract-beer-brewing-kit-5-gallons.html)
* [Extract Brewing Instructions](./files/extract-brewing-instructions.pdf) ([source](https://www.morebeer.com/images/file.php?file_id=57243))
* [Recipe Sheet](./files/we-like-space-haze-dipa-recipe-sheet.pdf) ([source](https://www.morebeer.com/images/file.php?file_id=58139))

# Terminology

**Time:** Kettle addition times are usually measured in terms of minutes
remaining in the boil.

* *Lag Time:* Amount of time between when the yeast is added to the wort and
  when it starts showing active fermentation (when you start seeing bubbles in
  the air lock).
* *Pitching:* The act of adding yeast to the wort.  Namely, you often measure
  the temperature of the wort at which you pitch the yeast.
* *Partial/Full Boil:* You boil 2-3 gallons in a *partial boil*, and it is
  supplimented with cold tap water in the carboy until it reaches 5 gallons in
  total (this introduces a possible source of contamination though).
  For a *full boil*, you boil 7.5+ gallons and add no tap water, but this
  requires a wort chiller.

## Temperatures

* 185F: Max temperature to add whirlpool hops
* 170F: Good lower boundary for keeping the beer sterile
* **160F:** Point at which beer becomes at risk of contamination
* 130F: Able to transfer wort to fermenter
* 80F: Max temperature for adding yeast
* 70F: Good temperature for fermentation

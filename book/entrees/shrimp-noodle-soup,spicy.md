# Spicy Shrimp Noodle Soup

([Original Recipe](https://triedtrue.us/SpicyShrimpNoodleSoup))

**Time:**

* Preparation: 30 min.
* Cooking: 1 hour

## Ingredients

### Shrimp Stock

* 1 lb. Shrimp (peeled, deveined)
* 4 T. butter
* 1 Onion (yellow)
* 10 Cloves Garlic (roughly chopped)
* 2 T. Ginger (ground)
* 8 C. Chicken Stock
* Salt &amp; Pepper to taste

### Shrimp Noodles

* 16 oz. Spaghetti
* 3 t. Sesame Oil
* 1 t. Vegetable Oil (or another neutral oil)
* 1/2 Onion (yellow)
* 1/2 T. Red Pepper Flakes
* ~1/2 T. (?) Old Bay
* 12 oz. Broccoli florets
* Salt &amp; Pepper to taste

## Instructions

### Shrimp Stock

1. Melt **butter** into soup pot over medium heat.
2. Once melted and frothy, add **onion**, **ginger**, **garlic**, **salt** &amp; **pepper**
3. Stir occasionally for *5-7 min*.
4. Add **chicken stock** to the pot
5. Bring to a boil
6. Reduce heat to a simmer and cook for 20 min.
7. Add **shrimp** and continue simmering until shrimp is cooked

### Shrimp Noodles

1. In another pot, cook **noodles**
2. Set noodles aside (coat in oil to prevent them from drying)
3. Over medium heat, add **sesame oil** and **vegetable oil**
4. Add **onion** and cook until soft
5. Add **broccoli** and **seasonings** and cook until broccoli is done (but not soft)
6. Combine with the completed ***Shrimp Stock*** over heat
7. Adjust seasoning combination as needed

## Serving

* Divide pasta between bowls
* Ladle stock on top

# Kristen's Chili Mac Casserole

[Original Recipe](https://www.plainchicken.com/wprm_print/45862)

## Ingredients

* 1 lb. Cavatappi Noodles
* 2 lbs. Taco Seasoned Ground Turkey
* 1 can (15 oz.) Diced Tomatoes (undrained)
* 1 can (10 oz.) Diced Tomatoes with Green Chiles (undrained)
* 1 can (6 oz.) Tomato Paste
* 1 can (16 oz.) Chili Beans (undrained)
* 2 C. Shredded Monterey Jack
* 1 Large Yellow Onion
* 1 Bell Pepper
* 8 Cloves Garlic
* 2 pkgs. (2 oz.) Chili Seasoning Mix
* 1.5 Tbsp. Salt

## Instructions

1. Preheat oven to **375F** and coat 9x13 baking dish with cooking spray.
2. In a large pot, cook pasta **al dente**.  Drain and cover for later.
3. In a pan, **slightly cook** meat and veggies.
4. Add contents of pan to noodles and combine with everything else.
5. Spread into the baking dish and top with cheese.
6. Cover with foil and bake for **30 minutes**.
7. Remove foil and bake for another **5 minutes**.
8. **Broil** for another **2 minutes**.
9. Let sit for 15 minutes before serving

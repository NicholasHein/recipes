# Nick's One-Pot Broccoli Pasta

([Original Recipe](https://www.instagram.com/p/B13-Gv3nR2l/?igshid=NjZiM2M3MzIxNA%3D%3D))

## Ingredients

* 1lb Spicy Italian Sausage Links (casings removed)

  *or Jimmy Dean Hot Sausage (tube)*
* 1lb Broccoli Florets
* 1 lb. Rotini (or Rigatoni) Pasta
* 1 Large Onion (chopped)
* 5 Cloves of Garlic
* 4 C. Chicken Stock

  Substitution (suggested):
    - 2 C. Cream of Chicken Soup
    - 2 C. Water
* 1/2 C. Grated Parmesan Cheese
* 1/4 C. Fresh Parsley (chopped)
* 2 tsp. Red Pepper Flakes
* 1 tsp. Salt
* Olive Oil

## Instructions

1. Fry sausage in a bit of olive oil until brown (should be broken into smaller chunks)
2. Add onion and cook until soft
3. Add garlic
4. Add 1lb broccoli florets and cook until tender
5. Combine red pepper flakes & salt
6. Mix with pasta
7. Mix with chicken stock
8. Once simmering, add Parmesan cheese and parsley
9. Stir before covering and cook 10-12 min.

# Nick's One-Pot, Cheesy, Spicy ???

## Ingredients

* 1lb Rotini noodles
* 10 oz. Mozzarella
* Veggies:
  - 1 Onion
  - 5 cloves of Garlic
  - 10 oz. Broccoli Flourettes
* Broth:
  - 4 cups of water
  - 8 tsp. chicken broth base
* Meat:
  - 14 oz. Kielbosa
  - 1 lb. Spicy Jimmy Dean's sausage
* Seasonings:
  - ~1 tbsp. Ground Tumeric
  - ~1/2 tbsp. Ground Cumin
  - ~1 tbsb. Ground Cinnamon
  - ~3/4 tbsp. Mint
  - ~1/2 tbsp. Thyme
  - 1 1/2 tbsp. Red Pepper Flakes
* Sauces:
  - 10 tbsp. Soy Sauce
  - 8 tbsp. Worschesire Sauce

## Instructions


1. Add olive oil (generously) to large pot
1. Fry onions & garlic
2. Add Jimmy Dean's sausage
3. Add chopped up Kielbosa & Broccoli
4. Add seasonings & sauces
5. Simmer
6. Add noodles and broth
7. Simmer
8. Cover and simmer on low heat for 25 minutes(???)
9. Once noodles are cooked, add cheese & stir

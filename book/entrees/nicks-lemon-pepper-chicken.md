# Nick's Lemon Pepper Chicken

([Original Recipe](https://icingonthesteak.com/recipe/creamy-lemon-pepper-garlic-chicken/))

## Ingredients

* **Chicken:**
  * 1.5(-ish) lbs. Chicken thighs *(cubed or whole)*
  * 4 Tbsp. Lemon Pepper Seasoning
  * 1/2 C. Flour
* **Sauce:**
  * 1 Onion *(thick slices)*
  * 1 Med. Head Broccoli
  * 2 tsp. Olive Oil
  * 2 Tbsp. Butter
  * 1 Head Garlic (crushed)
  * 1.5 C. Chicken Stock *(use broth base powder/cubes)*
  * 2.5 tsp. Lemon Pepper Seasoning
  * 1/2 C. Heavy Whipping Cream
  * 1/2 C. Shredded Cheese *(any kind)*
  * 6 oz. Lemon Juice
* **Garnish:**
  * 1 Large lemon *(or 2 med. limes)* (sliced)
  * Parsley
* 1 lb. Spaghetti noodles (*or some white rice?*)[^1]

[^1]: The original recipe was just "Creamy Lemon Pepper Garlic Chicken", so it
  did not call for noodles (or rice).  It is very good with or without it.

## Instructions

1. **Prepare Chicken:**
   1. Preheat oven to 375F
   2. Mix lemon pepper with flour in a mixing bowl
   3. Save some of the seasoning for later
   4. Cover chicken in seasoning
   5. In a pan on medium heat, add ~3 Tbsp. olive oil
   6. Add chicken to pan and cook for ~5 min.
   7. Turn chicken and sprinkle remaining coating onto chicken
   8. Cook for ~4 min.
   9. Move chicken to baking tray and bake for ~20 min.
2. **Prepare Sauce:** *(possibly reusing the same pan)*
   1. Add olive oil and butter on medium heat until butter melts
   2. Add onion and cook until almost soft
   3. Add broccoli and cook until almost done
   4. Add garlic and cook for ~30 seconds
   5. Add chicken stock and lemon pepper seasoning; bring to a boil
   6. Add heavy whipping cream and bring to a boil again
   7. Add shredded cheese and melt
   8. Reduce heat to a simmer and stir until sauce is smooth
   9. Remove from heat
   10. Add lemon juice and parsley; combine
   11. Cover the sauce if chicken is not done
3. Once chicken is done, add to sauce
4. Serve with lemon/lime **or** add to sauce as well

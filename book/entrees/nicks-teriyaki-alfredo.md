# Nick's Teriyaki Alfredo

## Ingredients

* 16 Oz. Spicy Italian Sausage (thick slices/cut in halves)
* 1 Yellow Onion (thick slices)
* 1/2+ Lb. Fresh Broccoli Florets
* 2 tsp. Salt
* 6 Cloves Garlic
* ~1/4 C. Teriyaki Sauce
* 16 Oz. Alfredo Sauce
* 12 Oz. Wide Egg Noodles
* 1 Tbsp. Fresh Parsley

## Instructions

1. Fry sausage in some olive oil
2. Once sausage is half-way cooked, add onion
3. (Now is about the time you should start on the noodles)
4. Once onion is half-way cooked, add broccoli & salt
5. Once broccoli is mostly cooked, add garlic
6. After ~1 min., add teriyaki and alfredo; turn to low temp.
7. Strain egg noodles when done and add back to pot
8. Add the sauce and parsley to the pot
9. Serve warm

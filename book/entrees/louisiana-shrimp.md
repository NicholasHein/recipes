# Louisiana Shrimp

**Time:**

* Preparation: ??? min.
* Cooking: ??? min.

## Ingredients

* 1 lb. Shrimp (peeled &amp; deveined)
* ~~1/2 lb. Andouille Sausage (diced)~~
* 1 lb. Linguine Pasta (cooked)
* 1 Yellow Onion (diced)
* 4 Cloves Garlic (minced)
* 2 Tbsp. Butter
* 1/2 C. Heavy Cream
* 1 C. Seafood Stock
* (???) White Cooking Wine
* 1 Tbsp. Worcestershire
* (???) Cajun Seasoning
* 3 Tbsp. Parsley
* 1 Tbsp. Basil
* 1 Tbsp. Thyme (chopped)
* 1 tsp. Paprika
* 2 Bay Leaves
* Flour
* Shredded Parmesan
* Salt
* Pepper

## Instructions

1. In a sauce pan, add **olive oil** over *medium-high* heat
2. ~~Fry **sausage** until crispy, remove and drain on paper towls, then set
   aside~~
3. Add **butter** and **wine** and bring to a simmer over *medium-high* heat
4. Add **onion** and cook until slightly tender
5. Add **garlic** and cook for *2 min.*
6. Add **shrimp** and season with **cajun seasoning**
7. Flip the shrimp when pink on one side and cook for *1 more min.*
8. Scoop shrimp out of pan and set aside, leaving the liquid in the pan
9. Add **heavy cream**, **seafood stock**, **worcestershire**, **basil**,
   **parsley**, **thyme**, **paprika**, and **bay leaves**.  Add **salt** and
   **pepper** to taste
10. Bring to a simmer and whisk in small sprinkles of **flour** until the sauce
    thickens
11. Lower heat to *medium-low* add toss the shrimp in sauce
12. ~~Add the sausage back~~
13. Combine with **cooked pasta** and add **parmesan** to taste

## Sources

* [Rachael Ray](https://www.foodnetwork.com/recipes/rachael-ray/louisiana-style-shrimp-recipe-2200899)
* [Reddit Post by u/Motor_River9417 on r/tonightsdinner](https://www.reddit.com/r/tonightsdinner/comments/168z9f6/i_call_it_cajun_shrimp_scampi_and_its_so_tasty/)

# Broccoli Alfredo Rice

Original recipe: <https://themccallumsshamrockpatch.com/wprm_print/17044>

## Ingredients

- 3 cups Chicken stock organic (or 1½ cup cream of chicken soup + 1½ cup water)
- 1lb Jimmy Dean spicy sausage or 1lb Kielbosa
- 2 cups Short-grain organic, washed, sorted
- 1 tablespoon virgin olive oil organic
- 1 tablespoon basil chopped (or ½ tablespoon dried basil)
- Kosher salt to own taste
- black pepper to own taste
- 2 tablespoons Unsalted butter
- 1 medium yellow onion diced
- 3-5 cloves garlic minced
- 1 head Organic fresh or frozen broccoli cut in bite-sized pieces
- 2½ cups Homemade or Pre-made Alfredo sauce
- ½ cup Quality grated Parmesan Cheese
- 1 pinch Hot pepper flakes optional

## Instructions

1. Wash and pre-sort the short-grain organic rice
2. Put the organic short-grain rice and measured stock into a large flat-bottomed pot with a tight-fitting lid on high heat Add 1 tablespoon olive oil, basil, kosher salt, freshly ground black pepper to the pot at this time. Stir a few times, cook for about 4 minutes on high heat before reducing heat to low. Cook for about 10 minutes. Do not remove the lid.
3. Meanwhile, place medium skillet on medium heat, add 2 tablespoons of butter, once melted add the diced yellow onion, cook 3-4 minutes before adding minced garlic. Cook for another 1-2 minutes. Remove from the heat.
4. At this time add the fresh organic broccoli, sauteed onions, and garlic to the large flat-bottomed pot.  Add meat. Add the Alfredo sauce, and lightly stir till ingredients are combined. It should be creamy looking but not too runny. Let cook on lowest heat setting covered with a tight-fitting lid until the broccoli is firm but tender and the rice is completely cooked about 6-8 minutes.
5. Add the grated Parmesan Cheese and stir the rice mixture, careful not to break down the rice grains. Serve while hot with a few optional pepper flakes

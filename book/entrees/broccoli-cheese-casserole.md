# Broccoli Cheese Casserole

* [Original Recipe 1](https://pin.it/5VCqXrL)
* [Original Recipe 2](https://www.plainchicken.com/wprm_print/32545)

## Ingredients

* 2 lbs. Broccoli Florets
* 1 can Cream of Mushroom Soup (26 oz.)
* 1 C. French Onion Dip (refrigerated; sour cream-based)
* 12 oz. Crispy Fried Onions
* 2 C. Cheddar Cheese (shredded)
* 1 tsp. Black Pepper (ground)
* ~~1 C. Milk~~

## Instructions

1. Preheat oven to **350F**
2. Combine *almost* everything in a *large* mixing bowl (use only **1/2 of the cheese and fried onion**)
3. Lightly coat baking dish with cooking spray and spread mixture evenly
4. **Bake 25 mins.**
5. Add remaining cheese and fried onions
6. **Bake 5-10 mins.** or until cheese is bubbly and onions are golden brown

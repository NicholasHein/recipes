# Cajun Rice

**Time:**

* Preparation: 30 min.
* Cooking: 30 min.

## Ingredients

* 1 lb. Andouille Sausage (sliced)
* 1 Yellow Onion (diced)
* 1 Bell Pepper (diced)
* 3 Cloves Garlic (minced)
* 1 Can Red Beans (drained)
* 2 Tbsp. Butter
* 1.5 C. Zatarain's Long Grain Rice (dry)
* 3 C. Chicken Broth
* 3 tsp. Tony Cachere's Creole Seasoning
* 1 tsp. Paprika
* 1/2 tsp. Thyme
* 1/2 tsp. Pepper
* 1/2 tsp. Salt
* 1 Bay Leaf
* Olive Oil

## Instructions

1. In a medium pot, fry **Andouille sausage** in olive oil over medium-high
   heat until nice and crispy
2. Remove sausage to a bowl once
3. Fry **onion**, **bell pepper**, and **garlic** until slightly tender
4. Remove veggies to the same bowl
5. Fry **beans** by themselves until slightly tender
6. Remove beans to the same bowl
7. Add **chicken broth** and stir until the pot is deglazed
8. Add the **butter**, **seasonings**, and **bay leaf** and bring to a boil
9. Reduce heat and add the **rice**
10. Cover the pot and cook until the rice is done (*20 min.*)
11. Let the cooked rice rest until it's a bit more firm (*5-10 min.*)
12. Combine the ingredients from the bowl with the rice
13. Serve after the rice has rehydrated the sausage

## Sources

* [The Anthony Kitchen](https://www.theanthonykitchen.com/cajun-rice/#recipe)
* [Reddit Post by u/Ti2m on r/instapot](https://www.reddit.com/r/instantpot/comments/kyouac/cajun_rice_delicious_and_super_easy/)

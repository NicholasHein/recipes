# Kristen's Karbonara

1. Prepare
  - Combine:
    - 4 full eggs + 4 yolks
    - 2/3 cup Parmesan
  - Boil:
    - 1 lb spaghetti
  - Fry:
    - 2 tsp olive oil (or 2 tbsp butter)
    - 1 onion; chopped
    - 3 clove garlic
    - 4 oz bacon; diced + [4 cups shredded chicken]
2. Add to frying pan:
  - Cooked pasta
  - Egg mixture
  - Salt (to taste)
  - Pepper (to taste)
  - Basil (1/4 cup dried *or* 1/2 cup fresh)
  - Italian Parsley (1/4 cup dried or 1/2 cup fresh)
5. Combine until eggs are cooked
6. Add more parmesan to taste

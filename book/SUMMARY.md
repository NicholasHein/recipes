# Summary

[Introduction](./README.md)

[//]: # (ANCHOR: toc)
# Recipes

- [Entrees](./entrees/index.md)
  - [Broccoli Alfredo Rice](./entrees/broccoli-alfredo-rice.md)
  - [Broccoli Cheese Casserole](./entrees/broccoli-cheese-casserole.md)
  - [Cajun Rice](./entrees/cajun-rice.md)
  - [Kristens Chili Mac Casserole](./entrees/kristens-chili-mac-casserole.md)
  - [Kristen Carbonara](./entrees/kristen-carbonara.md)
  - [Louisiana Shrimp](./entrees/louisiana-shrimp.md)
  - [Nick's Cheesy, Spicy, ???](./entrees/nicks-cheesy-spicy-something.md)
  - [Nick's Lemon Pepper Chicken](./entrees/nicks-lemon-pepper-chicken.md)
  - [Nick's One Pot Broccoli Pasta](./entrees/nicks-one-pot-broccoli-pasta.md)
  - [Nick's Teriyaki Alfredo](./entrees/nicks-teriyaki-alfredo.md)
  - [Spicy Shrimp Noodle Soup](./entrees/shrimp-noodle-soup,spicy.md)
- [Treats](./treats/index.md)
  - [Sons of Norway Lefse](./treats/lefse,sons-of-norway.md)

# Other

- [Brewing](./brewing/index.md)
  - [Terminology](./brewing/terminology.md)
  - [Supplies &amp; Equipment](./brewing/supplies-and-equipment.md)
  - [Cleaning](./brewing/cleaning.md)
  - [To-Do](./brewing/todo.md)
  - [We Like Space Haze DIPA](./brewing/morebeer/we-like-space-haze-dipa.md)
[//]: # (ANCHOR_END: toc)

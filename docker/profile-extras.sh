export LS_OPTIONS='--color=auto'
eval "$(dircolors)"
alias ls='ls $LS_OPTIONS'
alias ll='ls $LS_OPTIONS -lah'
alias l='ls $LS_OPTIONS -lA'

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

PS1="\[\e]0;\u@\h: \w\a\]${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[1;35m\]<$IMAGE_NAME>\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ "

if [ -v CARGO_HOME ]; then
	export PATH="$CARGO_HOME/bin:$PATH"
fi

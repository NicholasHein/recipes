srcdir = $(or ${S}, .)/book
outdir = $(or ${O}, build)

.PHONY: all
all:

obj-y :=
obj-y += entrees/broccoli-alfredo-rice.pdf
obj-y += entrees/broccoli-cheese-casserole.pdf
obj-y += entrees/cajun-rice.pdf
obj-y += entrees/kristen-carbonara.pdf
obj-y += entrees/kristens-chili-mac-casserole.pdf
obj-y += entrees/louisiana-shrimp.pdf
obj-y += entrees/nicks-cheesy-spicy-something.pdf
obj-y += entrees/nicks-lemon-pepper-chicken.pdf
obj-y += entrees/nicks-one-pot-broccoli-pasta.pdf
obj-y += entrees/nicks-teriyaki-alfredo.pdf
obj-y += entrees/shrimp-noodle-soup,spicy.pdf

obj-y += treats/lefse,sons-of-norway.pdf
${outdir}/treats/lefse,sons-of-norway.pdf: ${srcdir}/treats/images/lefse,sons-of-norway_original-recipe.jpg

all-objs := $(addprefix ${outdir}/, ${obj-y})

.PHONY: all
all: ${all-objs}

.PHONY: clean
clean:
	-@rm -f ${all-objs}

${outdir}/%.pdf: ${srcdir}/%.md
	@echo "PANDOC    $@"
	@pandoc --resource-path=$(<D):$(@D):$(CURDIR) -o "$@" "$<" >/dev/null

$(shell mkdir -p ${outdir} $(sort $(dir ${all-objs})))

# Recipes

## Getting Started

**Requirements:**

* [`mdbook`](https://github.com/rust-lang/mdBook)
* [`mdbook-pagetoc`](https://github.com/slowsage/mdbook-pagetoc)
* [`mdbook-alerts`](https://github.com/lambdalisue/rs-mdbook-alerts)
* [`mdbook-pandoc`](https://github.com/max-heller/mdbook-pandoc) (optional)

```sh
cargo install mdbook \
    mdbook-pagetoc \
    mdbook-alerts

#cargo install mdbook-pandoc
```

## Developing

```sh
# Build the Docker image
docker build --tag recipes:latest ./docker

# Run the docker image
docker run \
    --mount type=bind,source=$PWD,target=/work,readonly \
    -v /work/build \
    --workdir /work \
    --rm \
    -it recipes:latest \
    bash -l -c mdbook build
```
